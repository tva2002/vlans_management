from django.conf.urls import url
from vlansview import views

urlpatterns = [
    url(r'^$', views.get_tree_menu),
    url(r'^viewvlan/(?P<vlan>.*?)/$', views.show1),
    url(r'^search/$', views.search),
]