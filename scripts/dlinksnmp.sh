#!/bin/bash
#snmpwalk -c public -v1 $1 $2 | grep Hex
#DES-35xx DES-38xx DES-3026 DGS-3426G
#// тип файла – конфигурация свитча
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.6.3 i 3
#// тип передачи – передача по стеи
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.4.3 i 2
#// имя файла
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.5.3 s $1.cfg
#// направление передачи – upload
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.7.3 i 2
#// ip tftp сервера
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.3.3 a 192.168.110.1

#// начать заливку
snmpset -v2c -c private $1 1.3.6.1.4.1.171.12.1.2.1.1.8.3 i 3