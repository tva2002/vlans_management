#!/usr/bin/env python
import subprocess

host_s='localhost'
sql="select p2.address from hostgroup_host as p1, hosts as p2  where p1.host_id=p2.host_id and ( p1.hostgroup_id='5'  or p1.hostgroup_id='6' or p1.hostgroup_id='10' or p1.hostgroup_id='11');"

subprocess.call(["./add_cfg_fil.sh"])
r=subprocess.Popen(["mysql", "-h", host_s, "-u", "getvlans", "-D","monarch","-e",sql], stdout=subprocess.PIPE)
u2 = r.stdout.read().strip()
s =str(u2)
ps=s.lstrip("address\n")
ips=str(ps).split('\n')

for host in ips:
    oid_swtype= ".1.3.6.1.2.1.1.1.0"
    oid_swname=".1.3.6.1.2.1.1.5.0"
    na = subprocess.Popen(["snmpwalk","-v","1","-c","public",host,oid_swname], stdout=subprocess.PIPE)
    una = na.stdout.read().strip()
    print "\n"
    print str(host)+"-"+str(una)

    vr=subprocess.Popen(["snmpwalk","-v","1","-c","public",host,oid_swtype], stdout=subprocess.PIPE)
    uvr = vr.stdout.read().strip()
    st=str(uvr)

    a = (("DGS-3120" in st)or("DGS-3420" in st)or("DGS-3612" in st)or("DES-3200" in st))
    b = (("DES-30" in st)or("DES-35" in st) or ("DGS-3426G" in st)or("DES-38" in st))and("DES-3552" not in st)
    if a :
           ra=subprocess.Popen(["./dlinkDGS31.sh",host], stdout=subprocess.PIPE)
           u = ra.stdout.read().strip()
           print "a -"+st
           print str(u)
    if b :
          rb=subprocess.Popen(["./dlinksnmp.sh",host], stdout=subprocess.PIPE)
          u1 = rb.stdout.read().strip()
          print "b -"+st
          print str(u1)
