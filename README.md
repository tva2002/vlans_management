# Vlans_management

This project appeared as an additional tool to the existing monitoring system. It was based on Nagis2 + Monarch. There was a need to control vlans and their distribution over the network.

Now, when a new switch is added to the monitoring system, information about new vlans and descriptions of ports for them is automatically taken from it, using the snmp protocol. The switch configuration file is written to a remote server.

# Description

The project consists of 3 parts:
1. The script for saving the configuration files of the switches to the remote server - get_cfg_sw.py.
2. The script for collecting data on new vlans and describing the ports to them - snmpwalk1_4.py.
3. Web interface for displaying the received data - vlans.

# Screenshots

 <img align="center" width="100%" src="img/docs_html_m234e607b.jpg">
 <img align="center" width="100%" src="img/docs_html_m25942ae0.jpg">
 <img align="center" width="100%" src="img/docs_html_46f8b56f.jpg">
 <img align="center" width="100%" src="img/docs_html_46f8b56f.jpg">
 <img align="center" width="100%" src="img/docs_html_49e13cb0.jpg">
 <img align="center" width="100%" src="img/docs_html_m25942ae0.jpg">
 <img align="center" width="100%" src="img/docs_html_49e13cb0.jpg">
 <img align="center" width="100%" src="img/docs_html_mf0cf1d4.jpg">
 <img align="center" width="100%" src="img/docs_html_m25942ae0.jpg">
 <img align="center" width="100%" src="img/docs_html_mf0cf1d4.jpg">
